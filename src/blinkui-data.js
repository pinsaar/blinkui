//BlinkUI Data is the copyright of Bysness Inc.
let dataSources={};
class DataSource{
    constructor(resourceName,sourceName,{method='socket',socketSender=null,uri=''}){
        this.resourceName=resourceName;
        this.sourceName=sourceName;
        if(dataSources[this.resourceName+'/'+this.sourceName]){
            console.error('Possible replacement of data source',sourceName,this.resourceName);
        }
        dataSources[this.resourceName+'/'+this.sourceName]=this;
        this.method=method;
        this.socketSender=socketSender;
        this.uri=uri;
    }
    get(params={}){
        return new Promise((resolve,reject)=>{
            if(this.method==='socket' && this.socketSender){
                this.socketSender(`/${this.uri}`,{resourceName:this.resourceName,sourceName:this.sourceName,params}).then(resolve).catch(reject);
            }
            else{
                reject('Ignoring get request because source invalid',this.method,this.socketSender)
            }
        })
    }
    setRemote(){

    }
}